# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Liquidation(metaclass=PoolMeta):
    "Staff Liquidation"
    __name__ = 'staff.liquidation'
    department = fields.Many2One('company.department', 'Department',
        required=False, depends=['employee'])
    project = fields.Many2One('project.work', 'Project', domain=[
        ('type', '=', 'project'),
    ])

    @classmethod
    def __setup__(cls):
        super(Liquidation, cls).__setup__()

    @fields.depends('employee', 'department')
    def on_change_employee(self):
        if self.employee and self.employee.department:
            self.department = self.employee.department.id

    @fields.depends('contract', 'project')
    def on_change_contract(self):
        super(Liquidation, self).on_change_contract()
        if self.contract and self.contract.project:
            self.project = self.contract.project.id


class LiquidationGroup(metaclass=PoolMeta):
    "Liquidation Group"
    __name__ = 'staff.liquidation_group'

    def get_other_values(self, employee):
        value = super(LiquidationGroup, self).get_other_values(employee)
        value['project'] = employee.contract.project.id
        value['department'] = employee.department.id
        return value
